class ArquivosController < ApplicationController
   before_action :set_arquivo, only: [:show, :edit, :update, :destroy]
   
  def index
      @arquivos = Arquivo.all
   end
   
   def new
      @arquivo = Arquivo.new
      @palestras = Palestra.all
   end
   
   def create
      @arquivo = Arquivo.new(arquivo_params)
      if @arquivo.save
         redirect_to arquivos_path, notice: "The arquivo #{@arquivo.name} has been uploaded."
      else
         render "new"
      end
      
   end
   
   def destroy
      @arquivo.destroy
      redirect_to arquivos_path, notice:  "The arquivo #{@arquivo.name} has been deleted."
   end
   
   def set_arquivo
      @arquivo = Arquivo.find(params[:id])
    end
   
   private
      def arquivo_params
      params.require(:arquivo).permit(:name, :attachment, :palestra_id)
   end
   
end
