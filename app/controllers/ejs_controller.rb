class EjsController < ApplicationController
  before_action :set_ej, only: [:show, :edit, :update, :destroy]


  def index
    @ejs = Ej.all
  end
  
  def ranking
    @ejs = Ej.all
  end

  def show
  end
  
  def new
    @ej = Ej.new
    @federacaos = Federacao.all
  end

  def edit
    @federacaos = Federacao.all
  end

  def create
    @ej = Ej.new(ej_params)

    respond_to do |format|
      if @ej.save
        format.html { redirect_to @ej, notice: 'Empresa Junior registrada com sucesso.' }
        format.json { render :show, status: :created, location: @ej }
      else
        format.html { render :new }
        format.json { render json: @ej.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @ej.update(ej_params)
        format.html { redirect_to @ej, notice: 'Empresa Junior atualizada com sucesso.' }
        format.json { render :show, status: :ok, location: @ej }
      else
        format.html { render :edit }
        format.json { render json: @ej.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @ej.destroy
    respond_to do |format|
      format.html { redirect_to ejs_url, notice: 'Empresa Junior removida com sucesso.' }
      format.json { head :no_content }
    end
  end
    

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ej
      @ej = Ej.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ej_params
      params.require(:ej).permit(:nome, :faculdade, :federacao_id, :user, :federada)
    end
end