class FederacaosController < ApplicationController
  before_action :set_federacao, only: [:show, :edit, :update, :destroy]

  # GET /federacaos
  # GET /federacaos.json
  def index
    @federacaos = Federacao.all
  end
  
  def ranking
    @federacaos = Federacao.all
  end

  # GET /federacaos/1
  # GET /federacaos/1.json
  def show
  end

  # GET /federacaos/new
  def new
    @federacao = Federacao.new
  end

  # GET /federacaos/1/edit
  def edit
  end

  # POST /federacaos
  # POST /federacaos.json
  def create
    @federacao = Federacao.new(federacao_params)

    respond_to do |format|
      if @federacao.save
        format.html { redirect_to @federacao, notice: 'Federação registrada com suceesso.' }
        format.json { render :show, status: :created, location: @federacao }
      else
        format.html { render :new }
        format.json { render json: @federacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /federacaos/1
  # PATCH/PUT /federacaos/1.json
  def update
    respond_to do |format|
      if @federacao.update(federacao_params)
        format.html { redirect_to @federacao, notice: 'Federação atualizada com suceesso.' }
        format.json { render :show, status: :ok, location: @federacao }
      else
        format.html { render :edit }
        format.json { render json: @federacao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /federacaos/1
  # DELETE /federacaos/1.json
  def destroy
    @federacao.destroy
    respond_to do |format|
      format.html { redirect_to federacaos_url, notice: 'Federação removida com suceesso' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_federacao
      @federacao = Federacao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def federacao_params
      params.require(:federacao).permit(:nome, :federada)
    end
end
