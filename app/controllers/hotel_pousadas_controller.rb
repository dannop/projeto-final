class HotelPousadasController < ApplicationController
  before_action :set_hotel_pousada, only: [:show, :edit, :update, :destroy ]
  


  # GET /hotel_pousadas
  # GET /hotel_pousadas.json
  def index
    @hotel_pousadas = HotelPousada.all
  end

  # GET /hotel_pousadas/1
  # GET /hotel_pousadas/1.json
  def show
  end

  # GET /hotel_pousadas/new
  def new
    @hotel_pousada = HotelPousada.new
  end

  # GET /hotel_pousadas/1/edit
  def edit
    
    
  end

  # POST /hotel_pousadas
  # POST /hotel_pousadas.json
  def create
    @hotel_pousada = HotelPousada.new(hotel_pousada_params)

    respond_to do |format|
      if @hotel_pousada.save
        format.html { redirect_to @hotel_pousada, notice: 'Estabelecimento criado.' }
        format.json { render :show, status: :created, location: @hotel_pousada }
      else
        format.html { render :new }
        format.json { render json: @hotel_pousada.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hotel_pousadas/1
  # PATCH/PUT /hotel_pousadas/1.json
  def update
    
    respond_to do |format|
      if @hotel_pousada.update(hotel_pousada_params)
        format.html { redirect_to @hotel_pousada, notice: 'Estabelecimento foi atualizado.' }
        format.json { render :show, status: :ok, location: @hotel_pousada }
      else
        format.html { render :edit }
        format.json { render json: @hotel_pousada.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hotel_pousadas/1
  # DELETE /hotel_pousadas/1.json
  def destroy
    @hotel_pousada.destroy
    respond_to do |format|
      format.html { redirect_to hotel_pousadas_url, notice: 'Hotel foi deletado.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hotel_pousada
      
      @hotel_pousada = HotelPousada.find(params[:id])
      @quartos = Quarto.where(hotel_pousada_id: params[:id])
     
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hotel_pousada_params
      params.require(:hotel_pousada).permit(:nome, :quarto_id, :vagas)
    end
end
