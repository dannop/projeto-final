class PagesController < ApplicationController
  before_action :non_logged_user
  before_action :current_user
  
  def home
  end
  
  def evento
  end
  
  private
  
  def pages_params
    params.require(:user).permit(:photo)
  end
  
  
end
