class PalestrasController < ApplicationController
  before_action :palestrante_acc, only: [:edit, :update]
  before_action :normal_user_or_adm, only: [:edit,:update,:destroy]
  before_action :set_palestra, only: [:show, :edit, :update, :destroy, :inscricao, :cancelamento_inscricao]
  before_action :get_palestrantes, only: [:new, :edit]
  before_action :current_user, only: [:inscricao]

  def index
    @palestras = Palestra.paginate(:page => params[:page], :per_page => 4)
  end

  def show
  end

  def new
    @palestra = Palestra.new
    @palestrantes = User.where(palestrante: true)
  end

  def edit
    @palestrantes = User.where(palestrante: true)
  end

  def create
    @palestra = Palestra.new(palestra_params)

    respond_to do |format|
      if @palestra.save
        format.html { redirect_to @palestra, notice: 'Palestra criada.' }
        format.json { render :show, status: :created, location: @palestra }
      else
        format.html { render :new }
        format.json { render json: @palestra.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @palestra.update(palestra_params)
        format.html { redirect_to @palestra, notice: 'Palestra atualizada.' }
        format.json { render :show, status: :ok, location: @palestra }
      else
        format.html { render :edit }
        format.json { render json: @palestra.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @palestra.destroy
    respond_to do |format|
      format.html { redirect_to palestras_url, notice: 'Palestra removida.' }
      format.json { head :no_content }
    end
  end
  
  def inscricao
    @inscription = Inscricao.new
    @inscription.user_id = current_user.id
    @inscription.palestra_id = @palestra.id
    @inscription.save
    Palestra.update(@palestra.id, :vagas => Palestra.find(@palestra.id).vagas-1)
    redirect_to user_path(current_user.id)
  end
  
  def cancelamento_inscricao
    Inscricao.find_by(user_id: current_user, palestra_id: @palestra.id).destroy
    Palestra.update(@palestra.id, :vagas => Palestra.find(@palestra.id).vagas+1)
  
   @palestras = Palestra.where(id: Inscricao.select("palestra_id").where(user_id: current_user.id))
    respond_to do |format|
      format.js {}
      format.html{redirect_to current_user}
      
    end
      
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_palestra
      @palestra = Palestra.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def palestra_params
      params.require(:palestra).permit(:id, :name, :detalhe, :user_id, :vagas, :dia, :arquivo, :attachment)
    end
    
    def get_palestrantes
      @palestrantes = User.where(palestrante: true)    
    end
    
    def palestrante_acc
      if !current_user.palestrante
        redirect_to palestras_path
      end
    end
end