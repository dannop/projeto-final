class QuartosController < ApplicationController
  before_action :set_quarto, only: [:show, :edit, :update, :destroy, :reserva]
  before_action :set_hotel_pousada, only: [:show, :edit, :update]


  # GET /quartos
  # GET /quartos.json
  def index
     @quartos = Quarto.all
     @hotel_pousadas = HotelPousada.all
  end

  # GET /quartos/1
  # GET /quartos/1.json
  def show
    
      @pessoas = User.where(id: Cama.select("user_id").where(quarto_id: @quarto.id))
      # @pessoas = User.all
  end

  # GET /quartos/new
  def new
    @quarto = Quarto.new
    @quarto.hotel_pousada_id = params[:id]
    
    
  end

  # GET /quartos/1/edit
  def edit
  end

  # POST /quartos
  # POST /quartos.json
  def create
    @quarto = Quarto.new(quarto_params)
   
    
    respond_to do |format|
      if @quarto.save
        format.html { redirect_to @quarto, notice: 'Quarto was successfully created.' }
        format.json { render :show, status: :created, location: @quarto }
      else
        format.html { render :new }
        format.json { render json: @quarto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quartos/1
  # PATCH/PUT /quartos/1.json
  def update
    respond_to do |format|
      if @quarto.update(quarto_params)
        format.html { redirect_to @quarto, notice: 'Quarto foi atualizado.' }
        format.json { render :show, status: :ok, location: @quarto }
      else
        format.html { render :edit }
        format.json { render json: @quarto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quartos/1
  # DELETE /quartos/1.json
  def destroy
    @quarto.destroy
    respond_to do |format|
      format.html { redirect_to quartos_url, notice: 'Quarto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def reserva
    @cama = Cama.new
    @cama.casal = false
    @cama.user_id = current_user.id
    @cama.quarto_id = @quarto.id
    @cama.save
    
    Quarto.update(@quarto.id, :cama_solteiro => Quarto.find(@cama.quarto_id).cama_solteiro-1)
    redirect_to user_path(current_user.id)
  
    # Quarto.update(@quarto.id, :cama_casal => Quarto.find(@cama.quarto_id).vagas-1)
    # redirect_to user_path(current_user.id)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quarto
      @quarto = Quarto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quarto_params
      params.require(:quarto).permit(:nome, :cama_solteiro, :cama_casal, :hotel_pousada_id)
    end
 
    
    def set_hotel_pousada
      @hotel_pousadas = HotelPousada.all
    end

    def get_total_vagas
      #ainda não funciona
      @total_de_vagas = Quarto.where(hotel_pousada_id: 1).sum(:vagas)
    
    end
    
    
    
end
