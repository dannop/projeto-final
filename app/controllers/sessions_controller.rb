class SessionsController < ApplicationController
  before_action :session_params, only: :create
  before_action :logged_user, only: [:new, :create]
  
  def new
  end
  
  def create
    user = User.find_by(email: session_params[:email])
    if user && user.authenticate(session_params[:password])
      log_in(user)
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      #flash[:success] = "Fala ae!"
      # redirect_to user_path(user)
      redirect_to home_path
      
    else
      render :new
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to login_path
  end
  
  private
  
  def session_params
    @session_params = params.require(:session).permit(:email,:password)
  end
  

end
