class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :normal_user_or_adm, only: [:edit,:update,:destroy]
  before_action :logged_user, only: [:new, :create]
  before_action :non_logged_user, except: [:new, :create]
  before_action :get_ejs, only:[:new, :create, :edit, :update, :show]
  before_action :get_inscricoes, only: [:show]
  before_action :get_reservas, only: [:show]
  
  
  def index
    @users = User.paginate(:page => params[:page], :per_page => 7)
  end

  def show
    @user = User.find(params[:id])

  end

  def new
    @user = User.new
  end

  def edit
    
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        ej = Ej.find(@user.ej_id)
        ej.membros = ej.membros + 1
        ej.save
        
        federacao = Federacao.find(ej.federacao_id)
        federacao.membros = federacao.membros + 1
        federacao.save
        
        #flash[:info] = "Por favor verifique seu email para ativar a conta."
        format.html { redirect_to @user, notice: 'O cadastro foi finalizado sem problemas.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'Usuario atualizado.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user.destroy
    ej = Ej.find(@user.ej_id)
    ej.membros = ej.membros - 1
    ej.save
   
    federacao = Federacao.find(ej.federacao_id)
    federacao.membros = federacao.membros - 1
    federacao.save
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'Usuario foi removido.' }
      format.json { head :no_content }
    end
  end

  private
  
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:nome, :sobrenome, :cpf, :email, :photo, :ej_id, 
                                  :password, :password_confirmation, :palestrante, :adm,)
    end
    
    def get_ejs
          @ejs = Ej.all
    end
    
    def get_inscricoes
        @palestras = Palestra.where(id: Inscricao.select("palestra_id").where(user_id: current_user.id))
    end
    
    def get_reservas
      @reservas = Quarto.where(id: Cama.select("quarto_id").where(user_id: current_user.id))
    end
    
end
