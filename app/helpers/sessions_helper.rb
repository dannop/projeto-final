module SessionsHelper
  def log_in(user)
    session[:user_id] = user.id
  end 
  
  # Se houver alguem logado, retornara o usuario
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  
  # Se houver alguem logado, retorna true
  def logged_in?
    !current_user.nil?
  end
  
  # Retira o id do usuario logado do cookie de sessao e limpa o current user
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
  
  # Redirecionar para o perfil do Usuario, caso o mesmo já esteja logado
  def logged_user
    if logged_in?
      redirect_to user_path(current_user)
    end
  end
  
  # Redirecionar para a página de login se o user não estiver logado
  def non_logged_user
    if !logged_in?
      redirect_to login_path
    end
  end
  
  def normal_user_or_adm
      if current_user != @user && !current_user.adm
        redirect_to user_path(current_user)
      end
  end
  
  def palestrante_acc
      if current_user != @user && !current_user.palestrante
        redirect_to user_path(current_user)
      end
  end
  
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
end
