class Arquivo < ApplicationRecord
  belongs_to :palestra
  
  mount_uploader :attachment, AttachmentUploader
  validates :name, presence: true
end
