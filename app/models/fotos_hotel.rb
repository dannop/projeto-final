class FotosHotel < ApplicationRecord
  mount_uploader :foto, PictureUploader
  
  belongs_to :hotel_pousada
  
end
