class Palestra < ApplicationRecord
  belongs_to :user
  
  has_many :inscricaos
  has_many :arquivos, dependent: :destroy
  
  def deliver_mail
    UserMailer.notify_via_email(user, self).deliver_now
  end
  
  
end
