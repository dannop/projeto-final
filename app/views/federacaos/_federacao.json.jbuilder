json.extract! federacao, :id, :nome, :federada, :created_at, :updated_at
json.url federacao_url(federacao, format: :json)
