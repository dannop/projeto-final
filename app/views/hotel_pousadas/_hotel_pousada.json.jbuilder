json.extract! hotel_pousada, :id, :nome, :quarto_id, :vagas, :created_at, :updated_at
json.url hotel_pousada_url(hotel_pousada, format: :json)
