json.extract! quarto, :id, :nome, :vagas, :created_at, :updated_at
json.url quarto_url(quarto, format: :json)
