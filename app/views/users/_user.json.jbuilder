json.extract! user, :id, :nome, :sobrenome, :cpf, :email, :senha, :created_at, :updated_at
json.url user_url(user, format: :json)
