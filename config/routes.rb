Rails.application.routes.draw do

  get 'palestras/:id/inscricao', to: 'palestras#inscricao', as: :palestra_incricao
  post 'palestras/:id/inscricao', to: 'palestras#inscricao'
  delete 'cancelamento/:id' , to: 'palestras#cancelamento_inscricao', as: :cancelamento
  
  get 'hotel_pousada/:id/quarto', to: 'quartos#show', as: :detalhe_quarto
  get 'quartos/:id', to: 'quartos#reserva', as: :reserva_cama
  post 'quartos/:id', to: 'quartos#reserva'
  get 'quartos/new/:id' ,to: 'quartos#new', as: :new_quarto
 
  get '/', to: 'users#home', as: :root
  get 'home', to: "users#home", as: :home
  
  get '/evento', to: 'pages#evento', as: :evento
  get '/crud', to: 'pages#crud', as: :crud
  
  get 'federacaos/ranking', to: 'federacaos#ranking', as: :ranking_federacao
  get 'ejs/ranking', to: 'ejs#ranking', as: :ranking_ej
  
  #get 'palestras/:id/arquivos', to: 'arquivos#index', as: :arquivos
  #get 'palestras/:id/arquivos/new', to: 'arquivos#new', as: :new_arquivo
  #post 'palestras/:id/arquivos/new', to: 'arquivos#create'
  #delete 'palestras/destroy/arquivos', to: 'arquivos#destroy', as: :arquivo
  
  resources :hotel_pousadas
  resources :quartos
  resources :palestras
  resources :users
  resources :ejs
  resources :federacaos
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :arquivos, only: [:index, :new, :create, :destroy]
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #Rotas de SessionsController
  
  get 'login', to: 'sessions#new', as: :login
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy', as: :logout
  
end