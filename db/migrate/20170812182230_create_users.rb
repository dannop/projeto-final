class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :nome
      t.string :sobrenome
      t.string :cpf
      t.string :email
      t.boolean :adm, default: false
      t.boolean :palestrante
      t.string :password_digest
      
      t.references :ej, foreign_key: true
      t.timestamps
    end
  end
end
