class AddDescriptionToPalestra < ActiveRecord::Migration[5.1]
  def change
    add_column :palestras, :detalhe, :string
    add_column :palestras, :hora, :integer
  end
end
