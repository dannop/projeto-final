class CreateFederacaos < ActiveRecord::Migration[5.1]
  def change
    create_table :federacaos do |t|
      t.string :nome
      t.integer :membros, default: 0

      t.timestamps
    end
  end
end
