class CreateEjs < ActiveRecord::Migration[5.1]
  def change
    create_table :ejs do |t|
      t.string :nome
      t.string :faculdade
      t.boolean :federada, default: false
      t.references :federacao, foreign_key: true
      t.integer :membros, default: 0

      t.timestamps
    end
  end
end
