class AddVagasToPalestra < ActiveRecord::Migration[5.1]
  def change
    add_column :palestras, :vagas, :integer
    add_column :palestras, :dia, :integer
  end
end
