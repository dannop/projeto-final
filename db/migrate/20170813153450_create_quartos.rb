class CreateQuartos < ActiveRecord::Migration[5.1]
  def change
    create_table :quartos do |t|
      t.string :nome
      t.integer :cama_solteiro
      t.integer :cama_casal
      t.references :hotel_pousada, foreign_key: true
      t.timestamps
    end
  end
end
