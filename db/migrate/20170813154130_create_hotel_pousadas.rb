class CreateHotelPousadas < ActiveRecord::Migration[5.1]
  def change
    create_table :hotel_pousadas do |t|
      t.string :nome
      t.integer :vagas
    
      t.timestamps
    end
  end
end
