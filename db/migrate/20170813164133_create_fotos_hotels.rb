class CreateFotosHotels < ActiveRecord::Migration[5.1]
  def change
    create_table :fotos_hotels do |t|
      t.references :hotel, foreign_key: true
      t.string :foto

      t.timestamps
    end
  end
end
