class CreateCamas < ActiveRecord::Migration[5.1]
  def change
    create_table :camas do |t|
      t.boolean :casal
      t.references :quarto, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
