class CreateArquivos < ActiveRecord::Migration[5.1]
  def change
    create_table :arquivos do |t|
      t.string :name
      t.string :attachment
      t.references :palestra, foreign_key: true

      t.timestamps
    end
  end
end
