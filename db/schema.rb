# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170814144647) do

  create_table "arquivos", force: :cascade do |t|
    t.string "name"
    t.string "attachment"
    t.integer "palestra_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["palestra_id"], name: "index_arquivos_on_palestra_id"
  end

  create_table "camas", force: :cascade do |t|
    t.boolean "casal"
    t.integer "quarto_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["quarto_id"], name: "index_camas_on_quarto_id"
    t.index ["user_id"], name: "index_camas_on_user_id"
  end

  create_table "ejs", force: :cascade do |t|
    t.string "nome"
    t.string "faculdade"
    t.boolean "federada"
    t.integer "federacao_id"
    t.integer "membros", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["federacao_id"], name: "index_ejs_on_federacao_id"
  end

  create_table "federacaos", force: :cascade do |t|
    t.string "nome"
    t.integer "membros", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fotos_hotels", force: :cascade do |t|
    t.integer "hotel_id"
    t.string "foto"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hotel_id"], name: "index_fotos_hotels_on_hotel_id"
  end

  create_table "hotel_pousadas", force: :cascade do |t|
    t.string "nome"
    t.integer "vagas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inscricaos", force: :cascade do |t|
    t.integer "user_id"
    t.integer "palestra_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["palestra_id"], name: "index_inscricaos_on_palestra_id"
    t.index ["user_id"], name: "index_inscricaos_on_user_id"
  end

  create_table "palestras", force: :cascade do |t|
    t.string "name"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "detalhe"
    t.integer "hora"
    t.integer "vagas"
    t.integer "dia"
    t.index ["user_id"], name: "index_palestras_on_user_id"
  end

  create_table "quartos", force: :cascade do |t|
    t.string "nome"
    t.integer "cama_solteiro"
    t.integer "cama_casal"
    t.integer "hotel_pousada_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hotel_pousada_id"], name: "index_quartos_on_hotel_pousada_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "nome"
    t.string "sobrenome"
    t.string "cpf"
    t.string "email"
    t.boolean "adm", default: false
    t.boolean "palestrante"
    t.string "password_digest"
    t.integer "ej_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "photo"
    t.string "remember_digest"
    t.string "reset_digest"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.index ["ej_id"], name: "index_users_on_ej_id"
  end

end
