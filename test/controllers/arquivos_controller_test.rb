require 'test_helper'

class ArquivosControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get arquivos_index_url
    assert_response :success
  end

  test "should get new" do
    get arquivos_new_url
    assert_response :success
  end

  test "should get create" do
    get arquivos_create_url
    assert_response :success
  end

  test "should get destroy" do
    get arquivos_destroy_url
    assert_response :success
  end

end
