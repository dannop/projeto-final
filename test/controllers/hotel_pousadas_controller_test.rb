require 'test_helper'

class HotelPousadasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @hotel_pousada = hotel_pousadas(:one)
  end

  test "should get index" do
    get hotel_pousadas_url
    assert_response :success
  end

  test "should get new" do
    get new_hotel_pousada_url
    assert_response :success
  end

  test "should create hotel_pousada" do
    assert_difference('HotelPousada.count') do
      post hotel_pousadas_url, params: { hotel_pousada: { nome: @hotel_pousada.nome, quarto_id: @hotel_pousada.quarto_id, vagas: @hotel_pousada.vagas } }
    end

    assert_redirected_to hotel_pousada_url(HotelPousada.last)
  end

  test "should show hotel_pousada" do
    get hotel_pousada_url(@hotel_pousada)
    assert_response :success
  end

  test "should get edit" do
    get edit_hotel_pousada_url(@hotel_pousada)
    assert_response :success
  end

  test "should update hotel_pousada" do
    patch hotel_pousada_url(@hotel_pousada), params: { hotel_pousada: { nome: @hotel_pousada.nome, quarto_id: @hotel_pousada.quarto_id, vagas: @hotel_pousada.vagas } }
    assert_redirected_to hotel_pousada_url(@hotel_pousada)
  end

  test "should destroy hotel_pousada" do
    assert_difference('HotelPousada.count', -1) do
      delete hotel_pousada_url(@hotel_pousada)
    end

    assert_redirected_to hotel_pousadas_url
  end
end
